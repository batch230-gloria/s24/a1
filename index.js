let num = 2;
let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);


// ------------------------------------
let address = ["258", "Washington Ave NW", "California", "90011"];
let [streetNum, city, state, postalCode] = address
console.log(`I live at ${streetNum} ${city}, ${state} ${postalCode}`);


// ------------------------------------
let animal = {
	name: "Lolong",
	typeOfAnimal: "crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
};

let {name, typeOfAnimal, weight, length} = animal;
console.log(`${name} was a saltwater ${typeOfAnimal}. He weighed at ${weight} with a measurement of ${length}.`);



// ------------------------------------
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((perNum) =>{
	console.log(perNum);
})

let reducedNumber = numbers.reduce((acc,cur)=>{
	return acc + cur;
})
console.log(reducedNumber);



// ------------------------------------
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(myDog);